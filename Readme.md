# sips

This is a simple wrapper for the `sips` command that comes with Mac OS X for querying and modifying images.

The `sips` command has four modes (image query, profile query, image modification, profile modification) which are segregated into 4 separated classes in this module.

Each of these modules has methods for every flag that can be passed to the `sips` command.

This module was built and tested with Version 10.4.4 of the sips tool (as shipped with Mac OS X 10.10 and 10.11).

## Installation

	npm install sips

Use `npm install sips --save` to automatically add to the dependencies list in your `package.json` file.

## Usage

First you need to require the module.

	var Sips = require('sips');

You can now use any of the four main classes (imageQuery, imageModification, profileQuery, profileModification).

Basic usage in pseudocode:

	Sips.OneOfTheFourMainClasses(fileOrListOfFiles)
		.function1(arg1)
		.function2(arg1, arg2)
		.execute(optionalOutput, optionalCallback);

The main class takes a single file path or array of file paths as its sole parameter. It returns an object. This object has methods named like the corresponding command line functions. You can chain as many functions as you like. The function parameters correspond to the command line parameters as well. There are also functions for the short parameters (`.v()` is an alias for `.verify()`).

When all desired functions are added, use `.execute()` to run the command line function. With no parameters given the input files will be overwritten. If you pass a string as the first argument that will be used for the `--out` flag.

An optional second parameter can be passed with a callback function. This callback will be executed with a result object.

There is also a `.getCommand()` function: With no parameters it will return the function that would be executed. If passed a callback, that callback will be called with the function.

You can also specify the output file with `.out(outputPath)`.

All functions from the main class MUST be called with the number of arguments documented below. There are no optional attributes.
BUT if you pass `null` as any argument then that argument will be omitted, which will probably not make sips happy.
If ALL parameters for a function are `null`, then the function will not add parameters to the sips call.

### imageQuery

Available `sips` functions:

- `getProperty(key)` / `g(key)`
- `extractProfile(key)` / `x(key)`

Example:

	Sips.imageQuery(myFile)
		.getProperty('pixelHeight')
		.execute(function(result) {
			console.log('The image is '+result.properties.pixelHeight+' pixels high');
		});

### profileQuery

Available `sips` functions:

- `getProperty(key)` / `g(key)`
- `extractTag(tag, tagFile)` / `X(tag, tagFile)`
- `verify()` / `v()`

Example:

	Sips.profileQuery(myFile)
		.getProperty('all')
		.execute(function(result) {
			console.log('Profile properties:');
			console.log(result.properties);
		});

### imageModification

Available `sips` functions:

- `setProperty(key, value)` / `s(key, value)`
- `deleteProperty(key)` / `d(key)`
- `embedProfile(profile)` / `e(profile)`
- `embedProfileIfNone(profile)` / `E(profile)`
- `matchTo(profile)` / `m(profile)`
- `matchToWithIntent(profile, intent)` / `M(profile, intent)`
- `deleteColorManagementProperties()`
- `rotate(degreesCW)` / `r(degreesCW)`
- `flip(orientation)` / `f(orientation)`
- `cropToHeightWidth(pixelsH, pixelsW)` / `c(pixelsH, pixelsW)`
- `padToHeightWidth(pixelsH, pixelsW)` / `p(pixelsH, pixelsW)`
- `padColor(hexcolor)`
- `resampleHeightWidth(pixelsH, pixelsW)` / `z(pixelsH, pixelsW)`
- `resampleWidth(pixelsW)`
- `resampleHeight(pixelsH)`
- `resampleHeightWidthMax(pixelsWH)` / `Z(pixelsWH)`
- `addIcon()` / `i()`

Example:

	Sips.imageModification(['./in/a.png', './in/b.png'])
		.setProperty('format', 'jpeg')
		.resampleHeightWidthMax(200)
		.execute(outputDir, function(result) {
			if(result.code==0) {
				console.log('OK');
			} else {
				console.log('An error occured');
				console.log(result.stderr);
			}
		});

Additional convenience functions:

- `setOutputFormat(format)`: Using this function is identical to calling `setProperty('format', format)` but maps common format names to the more uncommon ones used internally by sips (currently: `jpg` to `jpeg` and `tiff` to `tif`).

### profileModification

Available `sips` functions:

- `setProperty(key, value)` / `s(key, value)`
- `deleteProperty(key)` / `d(key)`
- `deleteTag(tag)`
- `copyTag(srcTag, dstTag)`
- `loadTag(tag, tagFile)`
- `repair()`

Example:

	Sips.profileModification(myProfile)
		.setProperty('description', 'A very good profile')
		.execute(function(result) {
			if(result.code==0) {
				console.log('OK');
			} else {
				console.log('An error occured');
				console.log(result.stderr);
			}
		});

## TODO

- Write tests
- Expand documentation
- Build useful examples

## Future plans

- promise-based interface
- Argument validation
