var imageModification = require('./imageModification');
var imageQuery = require('./imageQuery');
var profileModification = require('./profileModification');
var profileQuery = require('./profileQuery');

var Sips = {
	imageModification: imageModification,
	imageQuery: imageQuery,
	profileModification: profileModification,
	profileQuery: profileQuery
};

module.exports = Sips;