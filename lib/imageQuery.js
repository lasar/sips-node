var sipsBase = require('./sipsBase');

var imageQuery = new sipsBase({
	resultIsPropertyList: true,
	functions: {
		// Output the property value for key to stdout.
		getProperty: { params: ['key'], short: 'g', resultIsPropertyList: true },

		// Get the embedded profile from image and write it to profile.
		extractProfile: { params: ['profile'], short: 'x' },
	}
});

module.exports = function(images, opts) {
	return new imageQuery(images, opts);
};