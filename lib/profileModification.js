var sipsBase = require('./sipsBase');

var profileModification = new sipsBase({
	functions: {
		// Set a property value for key to value.
		setProperty: { params: ['key', 'value'], short: 's' },

		// Remove a property value for key.
		deleteProperty: { params: ['key'], short: 'd' },

		// Remove the tag element from a profile.
		deleteTag: { params: ['tag'] },

		// Copy the srcTag element of a profile to dstTag.
		copyTag: { params: ['srcTag', 'dstTag'] },

		// Set the tag element of a profile to the contents of tagFile.
		loadTag: { params: ['tag', 'tagFile'] },

		// Repair any profile problems and log output to stdout.
		repair: { params: [] },

		out: { params: ['resultFileOrDir'], end: true }
	}
});

module.exports = function(images, opts) {
	return new profileModification(images, opts);
};