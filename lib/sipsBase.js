var spawn = require('child_process').spawn;

var sipsBaseFactory = function(conf) {
	var sipsBase = function(images, opts) {
		this.sipsBin = 'sips';
		this.params = [];
		this.imageParams = this.everythingToArray(images);
		this.endParams = [];
		this.resultIsPropertyList = false;
		if(typeof opts=='object') {
			for(var o in opts) {
				this[o](opts[o]);
			}
		}
		return this;
	};

	sipsBase.prototype.execute = function(out, callback) {
		if(typeof out=='function') {
			callback = out;
			out = undefined;
		}
		if(out && conf.functions.out) {
			this.addParam('out', out);
		}
		return this.runCommand(callback);
	};

	sipsBase.prototype.setBin = function(sipsBin) {
		this.sipsBin = sipsBin;
	};

	sipsBase.prototype.collectParams = function() {
		return this.params.concat(this.imageParams, this.endParams);
	};

	sipsBase.prototype.getCommand = function(callback) {
		var command = this.sipsBin+' '+this.collectParams().join(' ');
		if('function'===typeof callback) {
			callback(command);
			return this;
		} else {
			return command;
		}
	};

	sipsBase.prototype.runCommand = function(callback) {
		var self = this;
			sips = spawn(this.sipsBin, this.collectParams()),
			stdout = [],
			stderr = [];
		sips.stdout.on('data', function(data) {
			stdout.push(data.toString());
		});
		sips.stderr.on('data', function(data) {
			stderr.push(data.toString());
		});
		sips.on('close', function(code) {
			if(typeof callback=='function') {
				var result = {
					code: code,
					stdout: stdout.join("\n").split(/\n/),
					stderr: stderr.join("\n").split(/\n/),
					command: self.getCommand()
				};
				if(code==0 && self.resultIsPropertyList) {
					result.properties = self.parsePropertyList(stdout);
				}
				callback(result);
			}
		});
		return this;
	};

	sipsBase.prototype.parsePropertyList = function(result) {
		var l, parts,
			properties = {},
			lines = result.toString().split(/\n/);
		lines.shift();
		for(l in lines) {
			parts = lines[l].trim().split(/: /);
			if(parts.length>=2) {
				key = parts.shift();
				value = parts.join(': ');
				properties[key] = value;
			}
		}
		return properties;
	};

	sipsBase.prototype.everythingToArray = function(input) {
		var o, array = [];
		if(typeof input=='object') {
			for(o in input) {
				array.push(input[o]);
			}
		} else {
			array = [input];
		}
		return array;
	};

	sipsBase.prototype.addParam = function(key, args) {
		var newArgParts = [], i, allNull = true, whichParams;
		if(!conf.functions[key]) {
			throw new Error('Parameter "'+key+'" unknown!');
		}
		args = this.everythingToArray(args);
		if(args.length>conf.functions[key].params.length) {
			args = args.slice(0, conf.functions[key].params.length);
		}
		if(args.length<conf.functions[key].params.length) {
			throw new Error('Method '+key+' requires '+conf.functions[key].params.length+' parameters!');
		}
		whichParams = conf.functions[key].end ? 'endParams' : 'params';
		newArgParts.push('--'+key);
		for(i in args) {
			if(args.hasOwnProperty(i) && null!==args[i]) {
				newArgParts.push(args[i]);
			}
			if(null!==args[i]) {
				allNull = false;
			}
		}
		if(false===allNull) {
			for(i in newArgParts) {
				this[whichParams].push(newArgParts[i]);
			}
		}
		if(conf.functions[key].resultIsPropertyList) {
			this.resultIsPropertyList = true;
		}
		return this;
	};

	for(var f in conf.functions) {
		(function(functionKey, functionConf) {
			var fn = 'undefined'!==typeof functionConf.fn ? functionConf.fn : function() {
				return this.addParam(functionKey, arguments);
			};
			sipsBase.prototype[functionKey] = fn;
			if(functionConf.short) {
				sipsBase.prototype[functionConf.short] = sipsBase.prototype[functionKey];
			}
		})(f, conf.functions[f]);
	}

	return sipsBase;
};

module.exports = sipsBaseFactory;