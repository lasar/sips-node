var sipsBase = require('./sipsBase');

var profileQuery = new sipsBase({
	resultIsPropertyList: true,
	functions: {
		// Output the property value for key to stdout.
		getProperty: { params: ['key'], short: 'g', resultIsPropertyList: true },

		// Write a profile tag element to tagFile.
		extractTag: { params: ['tag', 'tagFile'], short: 'X' },

		// Verify any profile problems and log output to stdout.
		verify: { params: [], short: 'v' }
	}
});

module.exports = function(images, opts) {
	return new profileQuery(images, opts);
};