var sipsBase = require('./sipsBase');

var imageModification = new sipsBase({
	functions: {
		// Set a property value for key to value.
		setProperty: { params: ['key', 'value'], short: 's' },

		// Remove a property value for key.
		deleteProperty: { params: ['key'], short: 'd' },

		// Embed profile in image.
		embedProfile: { params: ['profile'], short: 'e' },

		// Embed profile in image only if image doen't have a profile.
		embedProfileIfNone: { params: ['profile'], short: 'E' },

		// Color match image to profile.
		matchTo: { params: ['profile'], short: 'm' },

		// Color match image to profile with rendering intent perceptual | relative | saturation | absolute.
		matchToWithIntent: { params: ['profile', 'intent'], short: 'M' },

		// Delete color management properties in TIFF, PNG, and EXIF dictionaries.
		deleteColorManagementProperties: { params: [] },

		// rotate
		rotate: { params: ['degreesCW'], short: 'r' },

		// flip
		flip: { params: ['orientation'], short: 'f' },

		// Crop image to fit specified size.
		cropToHeightWidth: { params: ['pixelsH', 'pixelsW'], short: 'c' },

		// Pad image with pixels to fit specified size.
		padToHeightWidth: { params: ['pixelsH', 'pixelsW'], short: 'p' },

		// Use this color when padding. White=FFFFFF, Red=FF0000, Default=Black=000000
		padColor: { params: ['hexcolor'] },

		// Resample image at specified size. Image apsect ratio may be altered.
		resampleHeightWidth: { params: ['pixelsH', 'pixelsW'], short: 'z' },

		// Resample image to specified width.
		resampleWidth: { params: ['pixelsW'] },

		// Resample image to specified height.
		resampleHeight: { params: ['pixelsH'] },

		// Resample image so height and width aren't greater than specified size.
		resampleHeightWidthMax: { params: ['pixelsWH'], short: 'Z' },

		// Add a Finder icon to image file.
		addIcon: { params: [], short: 'i' },

		out: { params: ['resultFileOrDir'], end: true },

		// Convenience function that maps to `--setProperty format [format]`
		// Used to convert common image format names to the ones used by sips.
		setOutputFormat: { params: ['format'], fn: function setOutputFormat(format) {
			format = format.toLowerCase();
			var formatMap = {
				'tif': 'tiff',
				'jpg': 'jpeg'
			};
			if('undefined'!==typeof formatMap[format]) {
				format = formatMap[format];
			}
			return this.addParam('setProperty', ['format', format]);
		} }
	}
});

module.exports = function(images, opts) {
	return new imageModification(images, opts);
};